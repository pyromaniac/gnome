# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2010 Pierre Lejeune <superheron@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix='tar.xz' ] meson [ meson_minimum_version=0.54.0 ]

export_exlib_phases src_prepare

SUMMARY="C++ wrapper for the libxml2 XML parser library"
HOMEPAGE="https://libxmlplusplus.github.io/libxmlplusplus/"

LICENCES="LGPL-2.1"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3.5]
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            dev-lang/perl:*
            dev-libs/libxslt
            media-gfx/graphviz
        )
    build+run:
        dev-libs/libxml2:2.0[>=2.7.7]
        gnome-bindings/glibmm:2.4[>=2.32]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbuild-deprecated-api=false
    -Dbuild-examples=false
    -Dbuild-pdf=false
    # "Validate the tutorial XML file", would need xmllint -> libxml2
    -Dvalidation=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc build-documentation'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dbuild-tests=true -Dbuild-tests=false'
)

libxml++_src_prepare() {
    meson_src_prepare

    # Fix docdir
    edo sed \
        -e "/install_docdir/s:/ book_name:/ '${PNVR}':" \
        -i docs/reference/meson.build
    edo sed \
        -e "/install_tutorialdir/s:/ book_name:/ '${PNVR}':" \
        -i docs/manual/meson.build
}

