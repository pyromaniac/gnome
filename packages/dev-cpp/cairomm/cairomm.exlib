# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require meson

SUMMARY="C++ bindings for Cairo"
HOMEPAGE="https://www.cairographics.org/${PN}/"
DOWNLOADS="https://www.cairographics.org/releases/${PNV}.tar.xz"

LICENCES="LGPL-2"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3.5]
        virtual/pkg-config[>=0.20]
        doc? (
            app-doc/doxygen
            dev-libs/libxslt
            media-gfx/graphviz
        )
    build+run:
        x11-libs/cairo[>=1.12.0]
    test:
        dev-libs/boost[>=1.63.0]
        media-libs/fontconfig
"

if ever at_least 1.18.0; then
    DEPENDENCIES+="
        build+run:
            dev-cpp/libsigc++:3[>=3.0.0]
            x11-libs/cairo[>=1.14.0]
    "
else
    DEPENDENCIES+="
        build+run:
            dev-cpp/libsigc++:2[>=2.6.0]
    "
fi

MESON_SRC_CONFIGURE_PARAMS=(
    -Dboost-shared=true
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc build-documentation'
)
MESON_SRC_CONFIGURE_TESTS=(
    '-Dbuild-tests=true -Dbuild-tests=false'
)

