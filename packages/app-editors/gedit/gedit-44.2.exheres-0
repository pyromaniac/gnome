# Copyright 2019 Calvin Walton <calvin.walton@kepstin.ca>
# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ]
require gsettings
require gtk-icon-cache
require freedesktop-desktop
require python [ blacklist=2 multibuild=false ]
require vala [ vala_dep=true ]
require meson


SUMMARY="Editor for the GNOME environment"
DESCRIPTION="
While aiming at simplicity and ease of use, gedit is a powerful general purpose text editor which
features full support for UTF-8, configurable highlighting for various languages and many other
features making it a great editor for advanced tasks.
"
LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk-doc [[ description = [ Install API reference for plugins ] ]]
"

DEPENDENCIES="
    build:
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.18]
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/amtk:5[>=5.6]
        dev-libs/atk
        dev-libs/glib:2[>=2.70.0]
        dev-libs/libpeas:1.0[>=1.14.1][python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
        gnome-desktop/gobject-introspection:1
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/gspell:1[>=1.0]
        gnome-desktop/gtksourceview:4.0[gobject-introspection][vapi]
        gnome-desktop/tepl:6[=6.4*] [[ note = [ tepl is not API or ABI stable ] ]]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.22.0][gobject-introspection]
        x11-libs/pango[gobject-introspection]
    suggestion:
        gnome-desktop/gedit-plugins [[ description = [ Plugins for additional features ] ]]
"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=( 'gtk-doc gtk_doc' )

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
    freedesktop-desktop_pkg_postrm
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
    freedesktop-desktop_pkg_postinst
}

