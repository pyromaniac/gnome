# Copyright 2023 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require cargo [ rust_minimum_version=1.70.0 ]
require meson
require gnome.org [ suffix=tar.xz ]

SUMMARY="Image loaders for the glycin image library"
HOMEPAGE="https://gitlab.gnome.org/sophie-h/glycin"

LICENCES="|| ( MPL-2.0 LGPL-2.1 )"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    heif [[ description = [ Load AVIF and HEIC images using libheif ] ]]
"

# The tests only support being run against the installed loaders
RESTRICT="test"

DEPENDENCIES="
    build+run:
        x11-libs/gtk:4.0[>=4.12.0]
        (
            dev-libs/glib:2
            dev-libs/libxml2:2.0
            x11-libs/cairo[>=1.17.0]
            x11-libs/gdk-pixbuf:2.0
            x11-libs/pango
        ) [[ note = [ glycin-svg ] ]]
        heif? ( media-libs/libheif[>=1.14.2] )
"

src_prepare() {
    meson_src_prepare

    # build system expects output directory without target name, but ecargo_config sets up the target
    # for cross-compilation
    edo sed -e "s:rust_target = 'release':rust_target = '$(rust_target_arch_name)/release':" -i loaders/meson.build
}

src_configure() {
    ecargo_config

    # Use dependency sources vendored in the tarball
    edo cat >> .cargo/config <<EOF
[source.crates-io]
replace-with = "vendored-sources"
[source.vendored-sources]
directory = "${MESON_SOURCE}/vendor"
[net]
offline = true
EOF

    local enabled_loaders="glycin-image-rs,glycin-jxl,glycin-svg"
    optionq heif && enabled_loaders+=",glycin-heif"

    exmeson -Dloaders="${enabled_loaders}"
}

