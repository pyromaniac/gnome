# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]
# New releases are only available on gitlab, keep gnome.org exlib for REMOTE_IDS
require gitlab [ prefix='https://gitlab.gnome.org' user='GNOME' suffix=tar.bz2 new_download_scheme=true ]
require gsettings
require meson

SUMMARY="Terminal Application for the GNOME Desktop"
HOMEPAGE="https://wiki.gnome.org/Apps/Terminal"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    nautilus-extension [[ description = [ build nautilus extension ] ]]
    gnome-shell [[ description = [ build gnome-shell search provider ] ]]
"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        dev-util/desktop-file-utils
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.20]
        gnome-shell? ( gnome-desktop/gnome-shell )
    build+run:
        dev-libs/atk
        dev-libs/vte:2.91[>=0.70.0][gnutls][providers:gtk3]
        dev-libs/glib:2[>=2.52.0]
        dev-libs/pcre2[>=10.0]
        gnome-desktop/gsettings-desktop-schemas[>=0.1.0]
        sys-apps/util-linux [[ note = [ provides uuid ] ]]
        x11-libs/gtk+:3[>=3.22.27]
        x11-libs/pango
        nautilus-extension? ( gnome-desktop/nautilus[>=43.0] )
"

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'nautilus-extension nautilus_extension'
    'gnome-shell search_provider'
)

src_prepare() {
    meson_src_prepare

    # Replace hardcoded 'lib' (results in /usr/lib) with libdir option
    edo sed -e "/^gt_systemduserdir/s:'lib':gt_libdir:" \
            -i meson.build
}

