# Copyright 2011 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2022 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gtk-icon-cache
require vala [ vala_dep=true with_opt=true ]
require meson

SUMMARY="interfaces for applications and libraries in GNOME to access the user's online accounts"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
"

DEPENDENCIES="
    build:
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.2] )

        dev-libs/libxslt
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.23]
        gtk-doc? ( dev-doc/gtk-doc[>=1.3] )
    build+run:
        core/json-glib
        dev-libs/glib:2[>=2.67.4]
        dev-libs/libsecret:1
        dev-libs/libxml2:2.0
        gnome-desktop/libsoup:3.0[>=3.0]
        net-libs/rest:1.0[>=0.9.0][providers:soup3]
        net-libs/webkit:4.1[>=2.33.1]
        sys-apps/dbus
        x11-libs/gtk+:3[>=3.19.12]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgoabackend=true
    -Dinspector=true

    -Dexchange=true
    -Dfedora=false
    -Dgoogle=true
    -Dimap_smtp=true
    -Dkerberos=false
    -Dlastfm=true
    -Dmedia_server=true
    -Downcloud=true
    -Dwindows_live=true
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
    'gobject-introspection introspection'
    vapi
)

