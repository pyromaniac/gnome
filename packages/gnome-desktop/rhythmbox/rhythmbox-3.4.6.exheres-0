# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] gsettings gtk-icon-cache freedesktop-desktop
require python [ blacklist="2 3.1" min_versions="3.2.3" with_opt=true multibuild=false ]
require meson

SUMMARY="An integrated music management application"
DESCRIPTION="
Rhythmbox is your one-stop multimedia application, supporting a music library,
multiple playlists, internet radio, and more.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    brasero [[ description = [ Master audio CDs using Brasero ] ]]
    daap [[ description = [ Share media across local network using Apple DAAP protocol ] ]]
    grilo [[ description = [ Play content scraped from online services using Grilo ] ]]
    gtk-doc
    ipod [[ description = [ Enable support for iPods ] ]]
    keyring
    libnotify
    mtp [[ description = [ Enable support for Microsoft's Media Transfer Protocol (MTP) ] ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.35.0]
        gnome-desktop/yelp-tools
        virtual/pkg-config[>=0.20]
        x11-proto/xorgproto
        gtk-doc? ( dev-doc/gtk-doc[>=1.4] )
    build+run:
        core/json-glib
        dev-db/tdb[>=1.2.6]
        dev-libs/glib:2[>=2.56.0]
        dev-libs/libpeas:1.0[>=0.7.3]
        dev-libs/libxml2:2.0[>=2.7.8]
        gnome-desktop/gobject-introspection:1[>=0.10.0]
        gnome-desktop/libgudev
        gnome-desktop/libsoup:2.4[>=2.42.0]
        gnome-desktop/totem-pl-parser[>=3.2.0]
        media-libs/gstreamer:1.0[>=1.4.0][gobject-introspection]
        media-plugins/gst-plugins-base:1.0[>=1.4.0][gobject-introspection]
        media-plugins/gst-plugins-good:1.0 [[ note = [ for auto audio sink ] ]]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0[>=2.18.0][gobject-introspection]
        x11-libs/gtk+:3[>=3.20.0][gobject-introspection]
        x11-libs/libX11
        x11-libs/pango
        brasero? ( gnome-desktop/brasero:3[>=2.31.5] )
        daap? ( media-libs/libdmapsharing:4.0[>=3.9.4] )
        grilo? ( gnome-desktop/grilo:0.3[>=0.3.1] )
        ipod? ( media-libs/libgpod[>=0.7.96] )
        keyring? ( dev-libs/libsecret:1[>=0.18] )
        libnotify? ( x11-libs/libnotify[>=0.7.0] )
        mtp? ( media-libs/libmtp[>=0.3.0] )
        python? (
            dev-libs/libpeas:1.0[python_abis:*(-)?]
            gnome-bindings/pygobject:3[>=3.0.0][python_abis:*(-)?]
        )
    test:
        dev-libs/check
"

RESTRICT="test" # requires X

DEFAULT_SRC_PREPARE_PATCHES=(
    "$FILES"/0001-build-Use-get_variable-function-instead-of-running-p.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    '-Dfm_radio=disabled'
    '-Dgudev=enabled'
    '-Dlirc=disabled'
    '-Dplugins_vala=disabled'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'gtk-doc gtk_doc'
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'brasero'
    'daap'
    'grilo'
    'ipod'
    'keyring libsecret'
    'libnotify'
    'mtp'
    'python plugins_python'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

