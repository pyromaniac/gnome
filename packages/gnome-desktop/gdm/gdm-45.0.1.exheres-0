# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gdm option-renames [ renames=[ 'xdmcp remote-login' ] ]
require meson
require udev-rules

PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    journald
    plymouth [[ description = [ enable display switching from plymouth ] ]]
    remote-login [[ description = [ add support for remote logins ] ]]
    tcpd [[ description = [ use tcp-wrappers to secure xdcmp requests ] requires = [ remote-login ] ]]
    wayland
    ( providers: elogind systemd ) [[
        *description = [ Session tracking provider ]
        number-selected = exactly-one
    ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.8.1]
        dev-libs/libxml2:2.0 [[ note = [ required for xmllint ] ]]
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.20]
    build+run:
        app-text/iso-codes
        dev-libs/glib:2[>=2.56.0]
        gnome-desktop/gobject-introspection:1[>=0.9.12]
        gnome-desktop/libgudev[>=232]
        media-libs/libcanberra[>=0.4][providers:gtk3]
        media-libs/fontconfig[>=2.5]
        sys-apps/accountsservice[>=0.6.35][gobject-introspection][providers:elogind?][providers:systemd?]
        sys-apps/keyutils
        sys-libs/pam
        (
            x11-libs/libX11
            x11-libs/libXau
            x11-libs/libxcb
            x11-libs/libXext
            x11-libs/libXrandr
            x11-libs/libXft
            x11-libs/libXi
            x11-libs/libXinerama
        ) [[ note = [ X dependencies ] ]]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=2.91.1]
        journald? ( sys-apps/systemd )
        providers:elogind? ( sys-auth/elogind )
        providers:systemd? ( sys-apps/systemd )
        plymouth? ( sys-boot/plymouth )
        remote-login? ( x11-libs/libXdmcp )
        tcpd? ( sys-apps/tcp-wrappers )
    run:
        gnome-desktop/gnome-keyring:1[pam] [[ note = [ provides pam_gnome_keyring ] ]]
        gnome-desktop/gnome-session
        gnome-desktop/gnome-settings-daemon:3.0
        gnome-desktop/gnome-shell[>=$(ever major)]
        x11-apps/xhost [[ note = [ gdm-x-session uses this ] ]]
        x11-server/xorg-server[>=1.12] [[ note = [ for multi-seat ] ]]
    test:
        dev-libs/check[>=0.9.4]
"

src_prepare() {
    meson_src_prepare

    echo "WantedBy=graphical.target" >> data/gdm.service.in
}

MESON_SRC_CONFIGURE_PARAMS=(
    '-Ddefault-pam-config=exherbo'
    '-Ddmconfdir=/etc/X11/dm'
    '-Dgdm-xsession=true'
    '-Dinitial-vt=1'
    '-Dipv6=true'
    '-Dlang-file=/etc/locale.conf'
    '-Dlibaudit=disabled'
    '-Drun-dir=/run/gdm'
    '-Dscreenshot-dir=/run/gdm'
    '-Dselinux=disabled'
    "-Dudev-dir=${UDEVRULESDIR}"
    '-Duser-display-server=true'
    '-Dxauth-dir=/run/gdm'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'journald systemd-journal'
    'tcpd tcp-wrappers'
    'wayland wayland-support'
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'plymouth'
    'remote-login xdmcp'
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:elogind -Dlogind-provider=elogind'
    'providers:elogind -Dsystemdsystemunitdir=no'
    'providers:elogind -Dsystemduserunitdir=no'
    'providers:systemd -Dlogind-provider=systemd'
)

